package com.Novare.SampleEcommerce.Configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
        
        httpSecurity.authorizeHttpRequests((authorizeHttpRequests) -> authorizeHttpRequests
        .anyRequest().permitAll()
        );

        httpSecurity.csrf((csrf) -> csrf.disable());

        return httpSecurity.build();
    }
}