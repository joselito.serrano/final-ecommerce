package com.Novare.SampleEcommerce.Controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Novare.SampleEcommerce.Repository.Product;
import com.Novare.SampleEcommerce.Service.APIResponse;
import com.Novare.SampleEcommerce.Service.ProductService;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @PostMapping("/")
    public Product addProduct(@RequestBody Product product){
        return productService.save(product);
    }

    // @PostMapping("/bulk")
    // public Product addProducts(@RequestBody List<Product> product){
    //     return productService.saveAll(product);
    // }

    @GetMapping("")
    public List<Product> getAllProduct(){
        return productService.findAll();
    }

    @GetMapping("/pagination/{offset}/{pageSize}")
    public APIResponse<List<Product>> getProductPagination(@PathVariable int offset, @PathVariable int pageSize) {
        Page<Product> productPage = productService.findProductByPagination(offset, pageSize);
        return new APIResponse(productPage.getSize(), productPage);
    }
}
