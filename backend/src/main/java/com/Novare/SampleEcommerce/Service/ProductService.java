package com.Novare.SampleEcommerce.Service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.Novare.SampleEcommerce.Repository.Product;

public interface ProductService {
    Product save(Product product);

    List<Product> findAll();

    Product findById(long id);

    void deleteById(long id);

    Product updateById(long id, Product product);

    Page<Product> findProductByPagination(int offset, int pageSize);

}
