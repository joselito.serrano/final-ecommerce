# Hard Requirements
1. Use spring boot to cater your backend services
2. Utilize the use of Spring MVC Pattern to organize your packages, codes and business logic
3. Use Maven to build your project
4. Use MySQL/PostgreSQL for the database
5. Use Angular and/or React for the Frontend
6. Think and apply of your own UIUX and be able to articulate about it during the DEMO
7. Conduct a daily scrum meeting (DSM) to ensure targets are met and issues are addressed on a timely manner
8. During the demo, group members should be able to demonstrate major participation in the development
9. Use real life examples and test data.
10. Final project Demo date: June 14 - 15 (3 groups per day) **code freeze on June 14.


# Requirements
| Feature               | Requirements                                                                                                                                                                       | Points |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|
| Login                 | As a user I can create an account / register to the application                                                                                                                    | 2      |
|                       | As a user I can login into the application                                                                                                                                         | 1      |
|  Product List         | As an admin I can upload product list using a template file (.csv)                                                                                                                 | 3      |
|                       | As an admin I can add another product using a form in the application                                                                                                              | 2      |
|                       | As an admin I can edit product details                                                                                                                                             | 1      |
|                       | All applicable business rules and validation should be applied                                                                                                                     | 1      |
| Display Product List  | As a User I can Search, Sort & Filter the displayed products base on my inputs and actions (category)                                                                              | 3      |
|                       | [Technical Requirement] Maximum displayed products per page is 10. Backend pagination should be applied                                                                            | 1      |
| Cart                  | As a user I can add, update & delete products into my cart                                                                                                                         | 3      |
|                       | As a user I can filter, sort & search products added to my cart                                                                                                                    | 3      |
|                       | [Technical Requirement] Maximum displayed products per page is 10. Backend pagination should be applied                                                                            | 1      |
|                       | All applicable business rules and validation should be applied                                                                                                                     | 1      |
| Cards                 | As a user I can add one or more card (Credit / Debit) into my account for payment related purposes                                                                                 | 2      |
|                       | All applicable business rules and validation should be applied                                                                                                                     | 1      |
| Checkout and Payments | As a user I can select products from my cart to checkout                                                                                                                           | 1      |
|                       | As a user I can select or add card to pay for the selected products for checkout                                                                                                   | 1      |
|                       | As a user I should receive an email confirmation about my purchase including the details of my purchase                                                                            | 1      |
|                       | [Technical Requirement] Card limit validation should be applied (e.g. you cannot checkout the products if the card does not have enough balance or already reached its card limit) | 1      |
|                       | All applicable business rules and validation should be applied                                                                                                                     | 1      |
| Payment History       | As a user I can search, sort and filter all of my payments history                                                                                                                 | 3      |
|                       | As a user I can export my payments and transaction history into a PDF file                                                                                                         | 2      |
|                       | [Technical Requirement] Maximum displayed products per page is 20. Backend pagination should be applied                                                                            | 1      |
| Reports               | As an admin, I should be able to download the total number of products bought per day, per month and per year per SKU                                                              | 3      |
|                       | As an admin, I should be able to see the number of products bought per user per day, month, year                                                                                   | 3      |
|                       | As an admin, I should be able to download reports to a PDF file                                                                                                                    | 3      |
| WOW                   | Aside from the requirements above Add a WOW factor to your application                                                                                                             | 3      |
| Bonus                 | Prepare a system architecture documentation                                                                                                                                        |        |
| Total points          |                                                                                                                                                                                    | 48     |

# Non Functional Requirements
| Feature         | Requirements                                                                                                                      | Points |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------------|--------|
| Design          | Create an ERD representing all tables and their relationships in the application                                                  | 1      |
|                 | Create Sequence diagrams for Authentication and Transaction Processes                                                             | 1      |
|  Infrastructure | Dockerize all applications and services that will be used                                                                         | 2      |
| Security        | Secure the application with OAuth2.0, Basic or JWT                                                                                | 2      |
| Maintainability | The codes should be properly stored in a source repository and feature/functionalities are branched and merged properly           | 1      |
|                 | All requirements should be stored and tracked in Gitlab Issues and updated regularly                                              | 1      |
|                 | Code should be testable and should have unit testing especially the business logic and transactions                               | 2      |
| Traceability    | Provide logs especially on the most crucial parts of the system. Use proper logging levels depending on the information displayed | 2      |
| Total points    |                                                                                                                                   | 12     |

# Point Definition
| Point | Description               |
|-------|---------------------------|
| 1     | Met the requirement       |
| 0     | Requirements were not met |
| .5    | Partially met             |