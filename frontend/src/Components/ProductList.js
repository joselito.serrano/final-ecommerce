import React, { Component } from "react";
import ProductService from "../Services/ProductService";

class ProductList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      offset: 0,
      pageSize: 20,
      totalPage: 0,
      totalProduct: 0,
    };
  }

//   componentDidMount() {
//     // ProductService.getProducts().then((res) => {
//     //     this.setState({products: res.data.response.content})
//     // })

//     ProductService.getProductsByPagination(
//       this.state.offset,
//       this.state.pageSize
//     ).then((res) => {
//       this.setState({ products: res.data.response.content });
//       this.setState({ totalPage: res.data.response.totalPages });
//       this.setState({ totalProduct: res.data.response.totalElements });
//     });
//   }

componentDidMount() {
    this.fetchProductsByPage(this.state.offset);
  }
  
  fetchProductsByPage(page) {
    ProductService.getProductsByPagination(page, this.state.pageSize).then((res) => {
      this.setState({
        products: res.data.response.content,
        totalPage: res.data.response.totalPages,
        totalProduct: res.data.response.totalElements,
        offset: page
      });
    });
  }

  renderPagination() {
    const pages = [...Array(this.state.totalPage).keys()];
    return (
      <div>
        {pages.map((page) => (
          <button
            key={page}
            onClick={() => this.fetchProductsByPage(page)}
            style={{
              fontWeight: page === this.state.offset ? "bold" : "normal",
            }}
          >
            {page + 1}
          </button>
        ))}
      </div>
    );
  }

  render() {
    return (
      <div>
        <h2>Products</h2>
        <div style={{ display: "flex", flexWrap: "wrap", gap: "8px" }}>
          {this.state.products.map((product) => (
            <div
              key={product.id}
              style={{
                padding: "8px",
                border: "2px grey solid",
                width: "160px",
              }}
            >
              <img
                // src={product.image}
                alt={product.image}
                style={{ width: "100%" }}
              />
              <p style={{ fontSize: ".8rem" }}>
                <strong>{product.name}</strong>
              </p>
              <p style={{ fontSize: ".6rem" }}>{product.description}</p>
              <p style={{ fontSize: ".6rem" }}>{product.price}</p>
              <p style={{ fontSize: ".6rem" }}>{product.genre}</p>
            </div>
          ))}
        </div>

        {this.renderPagination()}
      </div>
    );
  }
}

export default ProductList;
